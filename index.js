//console.log("Hello World");

// ARITHMETIC OPERATORS
let x = 10;
let y = 9;

// Addition Operator
let sum = x + y;
console.log("Result of Addition Operator: " + sum);

// Subtraction Operator
let difference = y - x;
console.log("Result of Subtraction Operator: " + difference);

// Multiplication Operator
let product = y * x;
console.log("Result of Product Operator: " + product);

// Division Operator
let quotient = y / x;
console.log("Result of Quotient Operator: " + quotient);

// Modulus Operator
let remainder = y % x;
console.log("Result of Modules Operator: " + remainder);


/*
	ASSIGNENT OPERATOR
		☼ Basic Assignment Operator (=)
			► adds the value of the right operand to a variable and assigns
				the result to the varialble
		☼ Addition Assignment Operator (+=)
*/
// Basic Assignment Operator (=)
let assignmentNumber = 8;

// Addition Assignment Operator
// Shorthand -- assignmentNumebr + 2;
assignmentNumber +=2;
console.log(assignmentNumber); // reult: 10

// Subtraction/Multiplication/Division/Modulo Assignment Operator

// Shorthand -- assignmentNumebr - 2;
assignmentNumber -=2;
console.log(assignmentNumber); // reult: 6

// Shorthand -- assignmentNumebr * 2;
assignmentNumber *=2;
console.log(assignmentNumber); // reult: 16

// Shorthand -- assignmentNumebr / 2;
assignmentNumber /=2;
console.log(assignmentNumber); // reult: 8

// Shorthand -- assignmentNumebr % 2;
assignmentNumber %=2;
console.log(assignmentNumber); // reult: 8

/*
	Multiple Operators & Parenthesis
		► when this operator applied in a statement. it follows the PEMDAS
		  (Paranthesis, Exponents, Multiplication, Division, Addition & SUbtaction) rule.
*/

let mdas = 1 + 2 - 3 * 4 / 5;
console.log("Result of the mdas operation: " + mdas); // result: 0.6000000000000001

let pemdas = 1 + (2-3)*(4/5);
console.log("Result of the pemdas operation: " + pemdas); //result: 0.19999999999999996

let totalPemdas = 5**2 + (10-2) / 2 * 3;
console.log("Result of the Total Pemdas operation: " + totalPemdas); //result: 37

// Increment & Decrement
// Operators that add or subtract values by 1 & reassigns the value of the 
// variable where the increment/decrement was applied to

let z = 1;

// Pre-Increment
	// The Value of 'z' is added by  avalue of one before returning
	// the value and storing it in the value "increment".
let increment = ++z;
console.log("Result of the pre-increment: " + increment); //result: 2
console.log(z); //result: 2

//Post-Increment
	// The value of 'z' is returned in the variable "increment" then
	// the value of 'z' is increased by one.
increment = z++;
console.log("Result of the Post Increment: " + increment); //result: 2
console.log(z); //Result: 3


//Pre-decrement
let decrement = --z;
console.log("Result of the pre-decrement: " + decrement); //result: 2
console.log(z); //result: 2

// Post-decement
decrement = z--;
console.log("Result of the Post Decrement: " + decrement); //result: 2
console.log(z); //Result: 1


/*
	TYPE OF COERCION
		► is the automatic or implicit conversion of values from one date
			type to another

		- This happens when operations are performed on different data types
			that would normally not be possible.

		- Adding/COncatenating a string and number  will result a string

*/
// Coercion
// - because Concatenating a different Data Type
let numA = '10';
let numB = 12;

let coercion = numA + numB;
console.log(coercion); // Result: 1012
console.log(typeof coercion); // Result: String Data Type

// Non-Coercion
// - because same Number data type
let numC = 16;
let numD = 14;

let nonCoercion = numC + numD;
console.log(nonCoercion); // 30
console.log(typeof nonCoercion); // Result: Number Data Type

// Adding Boolean and Number
/*
	The result is a number
	The Boolean "true" is associated the value of 1
	The Boolean "false" is associated the value of 0
*/
let numE = true + 1;
console.log(numE); // result: 2
console.log(typeof numE); // Result: Number Data Type

let numF = false + 1;
console.log(numF); // result: 1
console.log(typeof numE); // Result: Number Data Type

// Comparison Operator

let juan = 'juan';

// equality Operator (==)
/*
	Checks whether the operands are equal/have the same content
	Attempts to compare operands of different data types
	Return a boolean value
*/
console.log(1 == 1); // result: true
console.log(1 == 2); // result: false
console.log(1 == '1'); // result: true
console.log(1 == '2'); // result: false
console.log(0 == false); // result: true
console.log(0 == true); // result: false
// Compares two Strings that are the same
console.log('juan' == 'juan'); // result: True
//Compares a Strings with the variable "juan" declared above 
//Comparing a Two Strings is an case sensitive 
console.log('juan' == juan); // result: True

//Inequality Operator(!=)
/*
	Checks whether the operand are NOT equal/have different content
*/
console.log(1 != 1); // result: false
console.log(1 != 2); // result: True
console.log(1 != '1'); // result: True
console.log(0 != false); // result: False
console.log('juan' != 'juan'); // result: False
console.log('juan' != juan); // result: False

// Strict Equality Operator (===)
/*
	- Checks whether the operands are equal/have the same content
	- Also COMPARES the DATA TYPES of 2 values
	- 
*/
console.log(1 === 1); //TRUE
console.log(1 === 2); // FALSE
console.log(1 === "1"); //FALSE
console.log(0 === false); //FALSE
console.log('juan' === 'juan'); //TRUE
console.log('juan' === juan); //TRUE

// Strict Equality Operator (===)
/*
	- Checks whether the operands are NOT equal/have the same content
	- Also COMPARES the DATA TYPES of 2 values
	- 
*/
console.log(1 !== 1); //FALSE
console.log(1 !==2 ); // TRUE
console.log(1 !== "1"); // TRUE
console.log(0 !== false); // TRUE
console.log('juan'!=='juan'); // FALSE
console.log('juan'!== juan); // FALSE

// Relational Operators
let a = 50;
let b = 65;

// GT or Greater than operator ( > )
let isGreaterThan = a > b;
console.log(isGreaterThan); // FALSE

// LT or Less than operator ( < )
let isLessThan = a < b;
console.log(isLessThan); // TRUE

// GT or Greater than or equal operator ( >= )
let isGTorEqual = a >= b;
console.log(isGTorEqual); // FALSE

// LT or Less than or equal operator ( >= )
let isLTorEqual = a <= b;
console.log(isLTorEqual); // FALSE


let numStr = '30';
console.log(a > numStr); // TRUE

let str = 'twenty';
console.log(b >= str); // Nan FALSE

// Logical Operator

let isLegalAge = true;
let isRegistered = false;

// Logical AND Operator (&& - Double Ampersand)
// true && true = true
// true && false = false

let allRequirements = isLegalAge && isRegistered;
console.log(allRequirements); //Result: FALSE

// Logical OR Operator (|| - Double Pipe)
// true || true = true
// true || false = true
// false || false = false

let someReqMet = isLegalAge || isRegistered;
console.log(someReqMet); //Result: True 


// Logical NOT Operator (! - exclamation point)
// Returns the opposite value
let someReqNotMet = !isRegistered;
console.log(someReqNotMet); //Result: True 







